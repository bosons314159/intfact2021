#!/usr/bin/python3

from pi import pi
import sys

c = 0
f = open("pi_keys.db","w")
while True:
    sumx = 0
    _c = c
    while sumx < 31:
        sumx = sumx + int(pi[_c])
        _c = _c + 1
    if sumx == 31:
        f.write(str(c+1))
        f.write(",")
        f.write(str(_c-c))
        f.write("\n")
    c = c + 1
    if c >= len(pi):
        break
f.close()
