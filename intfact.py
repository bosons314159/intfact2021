#!/usr/bin/python3

from __future__ import print_function
import sys
from pi import pi
from e import e

MARKER7=7
MARKER3=3
MARKER2=2
def characterize(num):
    global MARKER7
    global MARKER3
    global MARKER2
    c = 0
    f = open("./pi.txt","r")
    g = open("./e.txt","r")
    l = len(num)
    mark3_count = 0
    mark2_count = 0
    res = []
    hit1 = 0
    hit2 = 0
    first = True
    while True:
        pk = str(f.read(1))
        if pk == '.':
            pk = str(f.read(1))
        nk = num[c % l]
        ek = str(g.read(1))
        if ek == '.':
            ek = str(g.read(1))
        mark3= False
        mark2= False
        if (c + 1) % MARKER3 == 0:
            mark3 = True
            if pk == nk:
                mark3_count = mark3_count + 1
        if (c + 1) % MARKER2 == 0:
            mark2 = True
            if ek == nk:
                mark2_count = mark2_count + 1
        if (c+1) % MARKER7 == 0:
            pp = False
            ee = False
            pp_hit_count = -1
            ee_hit_count = -1
            if mark3 == True and pk == nk:
                pp_hit_count = mark3_count
            if mark2 == True and ek == nk:
                ee_hit_count = mark2_count
            if mark3 == True and mark2 == True and pk == nk and ek == nk:
                hit1 = hit1 + 1
                hit2 = hit2 + 1
                res.append([pp_hit_count, ee_hit_count])
                delta = abs(hit1-hit2)
                #print([c+1,[hit1, hit2]])
                if first == True:
                    last_delta = delta
                    first = False
                else:
                    diff = abs(delta - last_delta)
                    input([c+1,diff])
                    last_delta = delta
            elif mark3 == True and pk == nk:
                hit1 = hit1 + 1
                res.append([pp_hit_count, None])
                #print(c+1)
            elif mark2 == True and ek == nk:
                hit2 = hit2 + 1
                res.append([None, ee_hit_count])
                #print(c+1)
        c = c + 1
    f.close()
    g.close()

if __name__ == "__main__":
    num = str(sys.argv[1])
    characterize(num)
